var socket = io.connect('http://localhost:3000');

socket.on('addvertiseResp', function (data) {
    $('#dataSent').modal('toggle');
    $('#addvertisePopup').fadeIn();
});

$('#add').click(function () {
   $(this).closest('form').find("input[type=text]").val("");
   $('#addNetworkPopup').fadeIn();
});

$('#view').click(function () {
   $(this).closest('form').find("input[type=text]").val("");
   $('#viewNetworkPopup').fadeIn();
   $.ajax({
      url:'db.json',
      type: 'GET',
      success: function (r){
         $('#viewNetworkPopup tbody').html('');
         $.each(r.nets, function(index, net){
            $('#viewNetworkPopup tbody').append('<tr><td>'+ net.ip +'</td><td>'+ net.hop +'</td><td>'+ net.type +'</td><td>'+ net.interface +'</td></tr>');
         });
      }
   });
});

$('#addvertise').click(function () {
   $.ajax({
      url:'db.json',
      type: 'GET',
      success: function (r){
         socket.emit('addvertise', r);
      }
   });
});

$('#dataSent').on('hide.bs.modal', function (e) {
  $('#addvertisePopup').fadeOut();
});

$('#dataSent').on('hidden.bs.modal', function (e) {
  $('#addvertisePopup').fadeOut();
});

$('.cancel').click(function() {
    $('.popupWrap').fadeOut();
});

$('#addNew').click(function (e) {
   e.preventDefault();
   var ip = $('#ip').val();
   var hop = $('#hop').val();
   var type = $('#type').val();
   var interface = $('#interface').val();

   var db;
   var obj = {
      ip: ip,
      hop: hop,
      type: type,
      interface: interface
   };

   $.ajax({
      url:'db.json',
      type: 'GET',
      success: function (r){
         db = r || {
            nets: []
         };
         db.nets.push(obj);
         socket.emit('nets', db);
         $('.cancel').click();
      },
      error: function (r){
         db = {
            nets: []
         };
         db.nets.push(obj);
         socket.emit('nets', db);
         $('.cancel').click();
      }
   });
});