var fs = require('fs');

var db = [];

module.exports = function (io) {
	io.on('connection', function (socket) {
		socket.on('nets', function (data) {
			console.log(data);
			var nets = data;
			nets = JSON.stringify(nets, null, 4);
			fs.writeFileSync('public/db.json', nets, { encoding: 'utf8' });
		});
		socket.on('addvertise', function (data) {
			if(data && data.nets){
				db = db.concat(data.nets);
				console.log(db);
				socket.emit('addvertiseResp', { hello: 'world' });
			}
			else console.log('error');
		});
		socket.on('checkDB', function () {
			console.log('check DB');
			socket.emit('checkDBResp', db);
		});
	});
}
